# catalog-metadata-py

Validate `catalog-metadata.yaml` file against [Catalog metadata model](https://gitlab.com/open-mobility-indicators/raw-indicator-api/-/blob/ba9d7df945498bb797fe324391127df719f80efe/omi_api/domain/entities/catalog.py#L21).

This validator is used in [catalog-metadata](https://gitlab.com/open-mobility-indicators/catalog-metadata) project CI to insure that catalog metadata file is always valid.

## Install

Using a virtual env:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```
