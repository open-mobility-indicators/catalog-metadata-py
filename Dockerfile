FROM python:3.9-slim-bullseye

# Env variables that configure Python to run in a container:
# do not keep dependencies downloaded by "pip install"
ENV PIP_NO_CACHE_DIR=1
# do not write "*.pyc" files
ENV PYTHONDONTWRITEBYTECODE=1
# do not buffer input/output operations, displaying prints and log messages immediately
ENV PYTHONUNBUFFERED=1

WORKDIR /app

# Needed to resolve indicator-api git dependency
RUN apt update -y && apt install -y git

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY validate_catalog_metadata.py ./

ENTRYPOINT [ "python", "validate_catalog_metadata.py" ]
