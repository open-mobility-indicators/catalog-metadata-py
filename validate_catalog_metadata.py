#!/usr/bin/env
import argparse
import logging
import sys
from pathlib import Path

import yaml
from omi_api.infra.gitlab.model import GitLabCatalogMetadata
from pydantic.error_wrappers import ValidationError

log = logging.getLogger(__name__)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "catalog_metadata_file", type=Path, help="catalog metadata file to validate"
    )
    args = parser.parse_args()

    if not args.catalog_metadata_file.is_file():
        parser.error(
            f"catalog metadata file not found: {str(args.catalog_metadata_file)!r}"
        )

    logging.basicConfig(level=logging.INFO, format="%(levelname)s:%(message)s")

    with args.catalog_metadata_file.open("rt", encoding="utf-8") as fd:
        try:
            yaml_obj = yaml.safe_load(fd)
            GitLabCatalogMetadata.parse_obj(yaml_obj)
            log.info("catalog metadata file is valid.")
        except yaml.parser.ParserError:
            log.exception("catalog metadata file is not YAML valid")
            return 1
        except ValidationError:
            log.exception(
                "catalog metadata file is not valid against catalog metadata model"
            )
            return 1


if __name__ == "__main__":
    sys.exit(main())
